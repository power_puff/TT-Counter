<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{

    protected $fillable = array('game_id');

    public $timestamps = false;

    public function Game(){
        return $this->belongsTo('App\Models\Game', 'game_id');
    }

}
