<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{

    protected $fillable = array('name');

    public function Players(){
        return $this->hasMany('App\Models\Player', 'team_id');
    }

}
