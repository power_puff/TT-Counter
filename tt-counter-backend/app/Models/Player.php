<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{

    protected $fillable = array('name', 'team_id');

    public function Team(){
        return $this->belongsTo('App\Models\Team');
    }

    public function Games()
    {
        return $this->belongsToMany('App\Models\Game', 'game_players');
    }

}
