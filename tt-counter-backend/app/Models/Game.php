<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{

    public function Sets(){
        return $this->hasMany('App\Models\GameSet', 'game_id');
    }

    public function GamePlayers()
    {
        return $this->belongsToMany('App\Models\Player', 'game_players');
    }

}
