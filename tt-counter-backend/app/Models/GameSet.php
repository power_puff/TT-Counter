<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GameSet extends Model
{

    protected $fillable = array('game_id', 'finished', 'result_team_1', 'result_value_1', 'result_team_2', 'result_value_2');

    public function Game(){
        return $this->belongsTo('App\Models\Game');
    }

}
