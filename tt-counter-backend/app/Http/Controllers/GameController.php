<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Illuminate\Http\Request;
use GrahamCampbell\Binput\Facades\Binput;

class GameController extends Controller
{

    public function index()
    {

        $games = Game::with('Sets')->with('GamePlayers')->get();
        $finalGames = collect(); // new collection

        $filters = Binput::all();
        foreach ($filters as $key => $value){ // ->where() for every URL-Parameter for filtering
            if(($key == "finished" || $key == "sets_finished") && $value == "1"){ // if filtering only allows finished games
                foreach ($games as  $key => $game){
                    if($game->Sets->where('finished', '0')->count() == 0){
                        $finalGames->push($game); // add found item to result
                        // this hacky fix resulted in a bug, that ?finished=1 resulted in false values, in the time no other workaround was found in time
                    }
                }
            }
        }

        return $finalGames->all();
    }

    public function show(Game $game)
    {
        return Game::with('Sets')->with('GamePlayers')->find($game->id); // return specific game by id
    }

    /* public function put(Request $request, Game $game)
    {
        $game->update($request->all());
        return response()->json($game, 200);
    }
    This was removed, because a Game has no properties, because of this, it will never be updates
    */

    public function create(Request $request) {
        //$data = json_decode($request->getContent(), true);
        return Game::create(); // a game has no properties yet => no data required to create one
    }

}
