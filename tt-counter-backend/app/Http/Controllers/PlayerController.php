<?php

namespace App\Http\Controllers;

use App\Models\Player;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Http\Request;

class PlayerController extends Controller
{

    public function index()
    {
        $player = Player::with('Team');

        $filters = Binput::all(); // ->where() for every URL-Parameter (e.g.: /players/?name=Test)
        foreach ($filters as $key => $value){
            $player->where($key, $value);
        }

        return $player->get();
    }

    public function show(Player $player)
    {
        return Player::with('Team')->find($player->id); // return specific player by id
    }

    public function put(Request $request, Player $player)
    {
        $player->update($request->all()); // update player by given data
        return response()->json($player, 200); // return the newly updates player
    }

    public function create(Request $request) {
        $data = json_decode($request->getContent(), true);
        return Player::create($data); // create player from request
    }

}
