<?php

namespace App\Http\Controllers;

use App\Models\GameSet;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Http\Request;

class GameSetController extends Controller
{

    public function index()
    {
        $gameSet = GameSet::with('Game');

        $filters = Binput::all(); // ->where() for every URL-Parameter in URL
        foreach ($filters as $key => $value){
            $gameSet->where($key, $value);
        }

        return $gameSet->get();
    }

    public function show(GameSet $gameSet)
    {
        return GameSet::with('Game')->find($gameSet->id); // shows a specific gameSet from id
    }

    public function put(Request $request, GameSet $gameSet)
    {
        $gameSet->update($request->all()); // update gameSet from request
        return response()->json($gameSet, 200); // return updates gameSet
    }

    public function create(Request $request) {
        $data = json_decode($request->getContent(), true);
        return GameSet::create($data); // Create gameSet from Content
    }

}
