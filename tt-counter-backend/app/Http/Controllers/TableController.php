<?php

namespace App\Http\Controllers;

use App\Models\Table;
use Illuminate\Http\Request;

class TableController extends Controller
{

    public function index(){
        return Table::with('Game')->with('Game.Sets')->with('Game.GamePlayers')->get(); // return list of tables with relations
    }

    public function show(Table $table)
    {
        return Table::with('Game')->with('Game.Sets')->with('Game.GamePlayers')->find($table->id); // return a specific table by  id
    }

    public function put(Request $request, Table $table)
    {
        $table->update($request->all()); // update the table => see wiki for examples
        return response()->json($table, 200);
    }

    public function create(Request $request) {
        $data = json_decode($request->getContent(), true);
        return Table::create($data); // create a new table with the specified values
    }

}
