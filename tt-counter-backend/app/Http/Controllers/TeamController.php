<?php

namespace App\Http\Controllers;

use App\Models\Team;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Http\Request;

class TeamController extends Controller
{

    public function index()
    {
        $team = Team::with('Players');

        $filters = Binput::all(); // getting all parameters for searching (e.g.: "localhost/api/teams/?name=Test")
        foreach ($filters as $key => $value){
            $team->where($key, $value); // ->where statement for every parameter
        }

        return $team->get(); // return model, laravel will serialize automatically
    }

    public function show(Team $team)
    {
        return Team::with('Players')->find($team->id); // return specific team to show
    }

    public function put(Request $request, Team $team)
    {
        $team->update($request->all()); //Update a team with in body specified values. See wiki for further examples
        return response()->json($team, 200);
    }

    public function create(Request $request) {
        $data = json_decode($request->getContent(), true); // create a team with the provided data
        return Team::create($data);
    }

}
