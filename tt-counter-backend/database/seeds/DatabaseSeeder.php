<?php

use App\Models\Game;
use App\Models\Player;
use App\Models\Table;
use App\Models\Team;
use App\Models\GameSet;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if($this->command->confirm('Really run seeder?', true) == false){
            $this->command->info('Canceled!');
            return;
        }
        $faker = \Faker\Factory::create();

        Eloquent::unguard();

        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->command->info("Truncating data...");
        Team::truncate();
        Player::truncate();
        Table::truncate();
        Game::truncate();
        GameSet::truncate();
        DB::table('game_players')->truncate();
        $this->command->info("Data truncated");

        //Teams
        $this->command->info("");
        $this->command->info("Creating teams...");
        for ($i = 0; $i < 2; $i++) {
            Team::create([
                'name' => $faker->company,
            ]);
        }
        $this->command->info("Teams created");


        //Players
        $this->command->info("");
        $this->command->info("Creating players...");
        for ($i = 0; $i < 10; $i++) {
            Player::create([
                'name' => $faker->name,
                'team_id' => ($i >= 5) ? 2 : 1,
            ]);
        }
        $this->command->info("Players created");


        //Tables
        $this->command->info("");
        $this->command->info("Creating tables...");
        for ($i = 0; $i < 3; $i++) {
            Table::create();
        }
        $this->command->info("Tables created");


        //Games
        $this->command->info("");
        $this->command->info("Creating games...");
        for ($i = 0; $i < 5; $i++) {
            Game::create();
        }
        $this->command->info("Games created");


        //Add GamePlayers
        $this->command->info("");
        $this->command->info("Creating GamePlayers...");
        for ($i = 1; $i < 6; $i++) {
            $game = Game::findOrFail($i);
            //attach players:

            $p = ($i > 4) ? 4 : $i;
            $game->GamePlayers()->attach(Player::where('team_id', 1)->skip($p)->first()->id);
            $game->GamePlayers()->attach(Player::where('team_id', 2)->skip($p)->first()->id);
        }
        $this->command->info("GamesPlayers created");


        //Add GameSets
        $this->command->info("");
        $this->command->info("Creating GameSets...");
        for ($i = 1; $i < 6; $i++) {
            $game = Game::findOrFail($i);
            //attach players:

            $variant = $faker->numberBetween(1, 1); //TODO: More variants, I know this is dirty
            if($variant == 1){
                //GameSet 1 (Team 1 won)
                GameSet::create([
                    'game_id' => $game->id,
                    'finished' => 1,
                    'result_team_1' => 1,
                    'result_value_1' => 11,
                    'result_team_2' => 2,
                    'result_value_2' => $faker->numberBetween(0,9),
                ]);

                //GameSet 2 (Team 2 won)
                GameSet::create([
                    'game_id' => $game->id,
                    'finished' => 1,
                    'result_team_1' => 2,
                    'result_value_1' => $faker->numberBetween(0,9),
                    'result_team_2' => 1,
                    'result_value_2' => 11,
                ]);

                if($faker->boolean()){
                    //GameSet 3 (Team 2 won)
                    GameSet::create([
                        'game_id' => $game->id,
                        'finished' => 1,
                        'result_team_1' => 2,
                        'result_value_1' => $faker->numberBetween(0,9),
                        'result_team_2' => 1,
                        'result_value_2' => 11,
                    ]);
                }else{
                    //GameSet 3 (Running game)
                    GameSet::create([
                        'game_id' => $game->id,
                        'finished' => 0,
                        'result_team_1' => 1,
                        'result_value_1' => $faker->numberBetween(0,9),
                        'result_team_2' => 2,
                        'result_value_2' => $faker->numberBetween(0,9),
                    ]);
                }
            }

        }
        $this->command->info("GameSets created");

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
