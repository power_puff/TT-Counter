<?php

use App\Models\Player;
use Illuminate\Database\Seeder;

class PlayerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 10;
        Player::truncate(); // delete all players
        $faker = \Faker\Factory::create(); // init faker

        $this->command->info("Creating {$count} players...");
        $this->command->getOutput()->progressStart($count);

        for ($i = 0; $i < $count; $i++) {
            Player::create([
                'name' => $faker->name,
                'team_id' => $faker->numberBetween(1, 2),
            ]); // fill database with faked player
            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
        $this->command->info("Created {$count} players");
    }
}
