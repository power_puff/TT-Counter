<?php

use Illuminate\Database\Seeder;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 2;
        \App\Models\Player::truncate();
        \App\Models\Table::truncate();
        $faker = \Faker\Factory::create();

        $this->command->info("Creating {$count} tables...");
        $this->command->getOutput()->progressStart($count);

        // create tables
        for ($i = 0; $i < $count; $i++) {
            \App\Models\Table::create();
            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
        $this->command->info("Created {$count} tables");
    }
}
