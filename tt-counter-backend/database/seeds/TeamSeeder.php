<?php

use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 2;
        \App\Models\Player::truncate(); // delete all players
        Team::truncate(); // delete all teams
        $faker = \Faker\Factory::create();

        $this->command->info("Creating {$count} teams...");
        $this->command->getOutput()->progressStart($count);

        for ($i = 0; $i < $count; $i++) {
            Team::create([
                'name' => $faker->company,
            ]); // created faked team
            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
        $this->command->info("Created {$count} teams");
    }
}
