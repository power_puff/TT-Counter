<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_sets', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('game_id');
            $table->boolean('finished');

            $table->integer('result_team_1');
            $table->integer('result_value_1');

            $table->integer('result_team_2');
            $table->integer('result_value_2');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_sets');
    }
}
