<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::get('users', 'UserController@index');
Route::get('user/{user}', 'UserController@show');
Route::put('user/{user}', 'UserController@put');*/

//$value = config('app.version');

Route::group(['prefix' => 'teams'], function () {
    Route::get('/', 'TeamController@index');
    Route::get('/{team}', 'TeamController@show');
    Route::put('/{team}', 'TeamController@put');
    Route::post('/', 'TeamController@create');
});

Route::group(['prefix' => 'players'], function () {
    Route::get('/', 'PlayerController@index');
    Route::get('/{player}', 'PlayerController@show');
    Route::put('/{player}', 'PlayerController@put');
    Route::post('/', 'PlayerController@create');
});

Route::group(['prefix' => 'games'], function () {
    Route::get('/', 'GameController@index');
    Route::get('/{game}', 'GameController@show');
    //Route::put('/{game}', 'GameController@put');
    Route::post('/', 'GameController@create');
});

Route::group(['prefix' => 'tables'], function () {
    Route::get('/', 'TableController@index');
    Route::get('/{table}', 'TableController@show');
    Route::put('/{table}', 'TableController@put');
    Route::post('/', 'TableController@create');
});

Route::group(['prefix' => 'gamesets'], function () {
    Route::get('/', 'GameSetController@index');
    Route::get('/{gameSet}', 'GameSetController@show');
    Route::put('/{gameSet}', 'GameSetController@put');
    Route::post('/', 'GameSetController@create');
});