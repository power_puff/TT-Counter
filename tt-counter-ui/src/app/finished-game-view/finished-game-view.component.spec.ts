import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishedGameViewComponent } from './finished-game-view.component';

describe('FinishedGameViewComponent', () => {
  let component: FinishedGameViewComponent;
  let fixture: ComponentFixture<FinishedGameViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishedGameViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishedGameViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
