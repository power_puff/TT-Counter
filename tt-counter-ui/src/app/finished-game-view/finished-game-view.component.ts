import {Component, OnInit} from '@angular/core';
import {GameService} from "../api/game.service";
import {Game, Set} from "../api/model";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
    selector: 'app-finished-game-view',
    templateUrl: './finished-game-view.component.html',
    styleUrls: ['./finished-game-view.component.scss']
})
export class FinishedGameViewComponent implements OnInit {

    public Games: Game[];
    public isLoading : boolean = false;
    public last_error : string = "";

    constructor(private gameService: GameService) {
    }

    ngOnInit() {
        this.refresh();
    }

    public getResult_Player1(game: Game): number {
        let count: number = 0;
        for (let set of game.sets) {
            if (set.result_value_1 > set.result_value_2) {
                count++;
            }
        }
        return count;
    }

    public getResult_Player2(game: Game): number {
        let count: number = 0;
        for (let set of game.sets) {
            if (set.result_value_2 > set.result_value_1) {
                count++;
            }
        }
        return count;
    }

    public refresh() : void{
        this.isLoading = true;
        this.last_error = "";
        this.gameService.getFinishedGames().subscribe(games => {
            this.Games = games;
            this.isLoading = false;
        }, error2 => {
            this.isLoading = false;
            this.last_error = error2.message;
        });
    }

}
