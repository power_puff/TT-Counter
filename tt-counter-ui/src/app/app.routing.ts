/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { Routes, RouterModule } from '@angular/router';

import { GametypeComponent } from './gametype-view/gametype-view.component';
import { HomeComponent } from './home/home.component';
import {AdminViewComponent} from "./admin-view/admin-view.component";
import {CounterViewComponent} from "./counter-view/counter-view.component";
import {CounterControlComponent} from "./counter-control/counter-control.component";
import {TeamLineupComponent} from "./team-lineup/team-lineup.component";
import {FinishedGameViewComponent} from "./finished-game-view/finished-game-view.component";


export const ROUTES: Routes = [
    {path:'', component: HomeComponent},
    {path: 'admin', component: AdminViewComponent, children: [
        {path: '', redirectTo: 'home', pathMatch: 'full'},
        {path: 'about', component: GametypeComponent},
        {path: 'team-lineup', component : TeamLineupComponent},
        {path: 'finished-game-view', component : FinishedGameViewComponent},
    ]},
    {path: 'counter', component: CounterViewComponent},
    {path: 'control', component: CounterControlComponent}
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(ROUTES);
