import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayTableViewComponent } from './play-table-view.component';

describe('PlayTableViewComponent', () => {
  let component: PlayTableViewComponent;
  let fixture: ComponentFixture<PlayTableViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayTableViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayTableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
