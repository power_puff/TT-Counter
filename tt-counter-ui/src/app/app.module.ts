import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { ClarityModule } from 'clarity-angular';
import { AppComponent } from './app.component';
import { ROUTING } from "./app.routing";
import { HomeComponent } from "./home/home.component";
import { GametypeComponent } from "./gametype-view/gametype-view.component";
import { CounterViewComponent } from './counter-view/counter-view.component';
import { CounterControlComponent } from './counter-control/counter-control.component';
import { AdminViewComponent } from './admin-view/admin-view.component';
import { TeamLineupComponent } from './team-lineup/team-lineup.component';
import { PlayTableViewComponent } from './play-table-view/play-table-view.component';
import {TableService} from "./api/table.service";
import {GameService} from "./api/game.service";
import {PlayerApiService} from "./api/player-api.service";
import {TeamApiService} from "./api/team-api.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { FinishedGameViewComponent } from './finished-game-view/finished-game-view.component';
import {GameViewService} from "./counter-view/game-view.service";

@NgModule({
    declarations: [
        AppComponent,
        GametypeComponent,
        HomeComponent,
        CounterViewComponent,
        CounterControlComponent,
        AdminViewComponent,
        TeamLineupComponent,
        PlayTableViewComponent,
        FinishedGameViewComponent
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        ClarityModule,
        ROUTING
    ],
    providers: [TableService, GameService, PlayerApiService, TeamApiService, GameViewService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
