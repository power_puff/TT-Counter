import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Table} from "./model";

@Injectable()
export class TableService {

  constructor(private http : HttpClient) { }

  public getAllTables() : Observable<Table[]> {
    return this.http.get<Table[]>("api/tables");
  }

  public getTable(id : number){
    return this.http.get<Table>(`api/tables/${id}`);
  }

  public updateTable(table : Table) : Observable<Table>{
   return this.http.put<Table>(`api/tables/${table.id}`, table);
  }



}
