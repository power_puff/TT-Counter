import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Player} from "./model";

@Injectable()
export class PlayerApiService {

  constructor(private http : HttpClient) { }

  public getAllPlayers() : Observable<Player[]> {
    return this.http.get<Player[]>("api/players");
  }

    public getPlayer(id : number) : Observable<Player> {
        return this.http.get<Player>(`api/players/${id}`);
    }

    public savePlayer(player : Player) : Observable<Player> {
        return this.http.post<Player>("api/players", player);
    }

    public updatePlayer(player : Player) : Observable<Player> {
        return this.http.put<Player>(`api/players/${player.id}`, player);
    }


}
