import { TestBed, inject } from '@angular/core/testing';

import { TeamApiService } from './team-api.service';

describe('TeamApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeamApiService]
    });
  });

  it('should be created', inject([TeamApiService], (service: TeamApiService) => {
    expect(service).toBeTruthy();
  }));
});
