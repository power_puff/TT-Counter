import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Game} from "./model";

@Injectable()
export class GameService {

  constructor(private http : HttpClient) { }

  public getAllGames() : Observable<Game[]> {
    return this.http.get<Game[]>("api/games");
  }

  public getFinishedGames() : Observable<Game[]> {
      return this.http.get<Game[]>("api/games?sets_finished=1");
  }
}
