export class Table {
    public id : number;
    public game : Game;
}

export class Game {
    public id : number;
    public sets : Set[];
    public game_players : Player[];
}

export class Set {
    public id : number;
    public finished : number;
    public result_team_1 : number;
    public result_value_1 : number;
    public result_team_2 : number;
    public result_value_2 : number;
}

export class Player {
    public id : number;
    public name : string;
    public team_id : number;
}

export class Team {
    public id : number;
    public name : string;
    public players : Player[];
}