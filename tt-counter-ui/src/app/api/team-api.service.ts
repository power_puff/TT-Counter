import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Team} from "./model";

@Injectable()
export class TeamApiService {

  constructor(private http: HttpClient) { }

  public getAllTeams() : Observable<Team[]> {
    return this.http.get<Team[]>("api/teams");
  }

  public getTeam(id : number) : Observable<Team>{
      return this.http.get<Team>(`api/teams/${id}`);
  }

    public saveTeam(team : Team) : Observable<Team>{
        return this.http.post<Team>("api/teams", team);
    }

    public updateTeam(team : Team) : Observable<Team>{
        return this.http.put<Team>(`api/teams/${team.id}`, team);
    }


}
