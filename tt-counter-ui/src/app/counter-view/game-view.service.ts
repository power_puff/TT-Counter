import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {TableService} from "../api/table.service";
import {Game, Player, Team} from "../api/model";
import {TeamApiService} from "../api/team-api.service";
import "rxjs/add/observable/timer";
import "rxjs/add/operator/do";
import "rxjs/add/operator/mergeMap";
import {Subject} from "rxjs/Subject";
import "rxjs/add/observable/interval";


@Injectable()
export class GameViewService {

    private teams: Team[]

    constructor(private tableService: TableService, private teamService: TeamApiService) {

    }

    public observeGameForPlate(plateId: number): Observable<ViewGame> {
        let $timer = Observable.interval(1000);

        let subject = Subject.create();

        let timer = setInterval(() => {
            this.getViewGame(plateId).subscribe(game => {
                subject.next(game);
            }, error => {
                subject.error(error);
            });
        }, 1000);
        
        return $timer.flatMap(number => this.getViewGame(plateId));
    }

    private getViewGame(plateId: number): Observable<ViewGame> {
        return this.getGame(plateId).map(game => {
            let map = new Map();
            game.game_players.forEach(player => {
                if (!map.has(player.team_id)) {
                    map.set(player.team_id, [])
                }
                map.get(player.team_id).push(player);
            });
            let viewGame = new ViewGame();
            viewGame.game = game;
            viewGame.playersPerTeam = map;

            return viewGame;
        });
    }

    private getGame(plateId: number): Observable<Game> {
        let game = this.tableService.getTable(plateId).map(table => {
            return table.game;
        });
        return game;
    }


}

export class ViewGame {
    public game: Game;
    public playersPerTeam: Map<number, Player[]>;
}
