import { TestBed, inject } from '@angular/core/testing';

import { GameViewService } from './game-view.service';

describe('GameViewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameViewService]
    });
  });

  it('should be created', inject([GameViewService], (service: GameViewService) => {
    expect(service).toBeTruthy();
  }));
});
