import {Component, OnDestroy, OnInit} from '@angular/core';
import {GameViewService} from "./game-view.service";
import {Observable} from "rxjs/Observable";
import {Game, Player, Set} from "../api/model";
import {Subscription} from "rxjs/Subscription";
import {error} from "selenium-webdriver";
import {current} from "codelyzer/util/syntaxKind";


@Component({
    selector: 'app-counter-view',
    templateUrl: './counter-view.component.html',
    styleUrls: ['./counter-view.component.scss']
})
export class CounterViewComponent implements OnInit, OnDestroy {

    private gameSubscription: Subscription;

    game: Game;

    private plateId: number = 1;

    error;

    players;

    setToPlay: Set;

    finishedSets: Set[];

    team1: number

    team2: number

    sets: { result1: number, result2: number }[]

    constructor(private gameViewService: GameViewService) {
    }

    ngOnInit() {
        this.gameSubscription = this.gameViewService.observeGameForPlate(this.plateId).subscribe(game => {

            this.players = [];
            this.setToPlay = null;
            this.finishedSets = [];
            game.playersPerTeam.forEach(players => {
                this.players.push(players.map(player => player.name).join(" | "));
            });
            this.finishedSets = game.game.sets.filter(set => {
                set.finished
            });
            this.setToPlay = game.game.sets.reduce((previous, current, index, array) => {
                if (!current.finished) {
                    return current
                }
            });
console.log(this.setToPlay);
            this.error = null;

        }, error => {
            this.error = "Fehler bei der Verbindung !!";
        })
    }

    ngOnDestroy(): void {
        this.gameSubscription.unsubscribe()
    }

    prepareGame() {
        // let team1 = new Team("TestTeam", ["Christian", "Sebastian"]);
        // let team2 = new Team("TestTeam2", ["Christian", "Sebastian"]);
        // let setToPlay = new Set(2,3);
        // let set1 = new Set(11,4);

        //this.game = new Game(team1, team2, setToPlay, [set1, set1]);
    }

}
