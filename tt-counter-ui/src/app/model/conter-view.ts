

export class ViewGame {
    constructor() {}
}

export class Team {

    constructor( public name : string,  public playerNames : string[]) {}
}

export class Set {
    constructor( public teamResult1 : number,  public teamResult2 : number) {}
}