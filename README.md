# TT-Counter

## Backend (Laravel)
The backend (REST-API) for the project, was written in [Laravel (v5.5)](https://laravel.com/docs/5.5).

The API only really does one job at the moment, it is just a "dumb" API, which will make everything you say him, as long as you call it correctly.

A documentation for the REST-Calls is in the [Wiki](https://gitlab.com/power_puff/TT-Counter/wikis/home), documentation for code can be found in the Controllers for the specific routes.


### Example (Player-Route)
All API-Routes are defined in `routes/api.php` this file will tell which url and request-type should be handeled by which controller and function.

![PlayerRoutes](https://i.imgur.com/YyvPHv5.png)

`Route::group` with a specified `prefix` will tell Laravel, that all routes inside the curly braces will have a `prefix` of `/Players`.

That said, the `Route::get('/')` would point to `/Players/` and, in this case will be handeled in the `index`-function inside the `PlayerController`.

![PlayerController](https://i.imgur.com/nI4jeoh.png)

Basically this function will load all players with their team, will apply filters and then will return the reult to the caller.

On API-Function return values, only the Model or a collection of a model can be returned and will automatically be converted to JSON (or another output-type) automatically.